*** Setting ***
Library    OperatingSystem
Library    Collections
Library    String
Library    RequestsLibrary


*** Variables ***
${URL_GET}    {ROBOT_HOST}:{ROBOT_PORT}

*** Keywords ***
Response Status should be Success
    [Arguments]    ${resp}
    log to console    ${resp.status_code}
    Should Be True    '${resp.status_code}' == '200'   
    #log to console    ${resp.content}
    ${resp_data}=    Set Variable    ${resp.content}
    ${resp_json}=    Set Variable    ${resp.json()}
    log to console    ===========> Get JSON <=========
    #log to console    ${resp_json}
    #FOR    ${item}    IN    @{resp_json}
        log to console    ${resp_json}
    #END

*** Test Cases ***
Get ARI TEST
    Create Session    Get_Address    ${URL_GET}
    &{params}=    Create Dictionary    Content-Type=application/json
    ${Response}=    Get Request    Get_Address    /getAddress    params=${params}
    Response Status should be Success    ${Response}
    
    
